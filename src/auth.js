// auth.js
var passport = require("passport");
var passportJWT = require("passport-jwt");
var Employe = require("./models/employe.js");
var cfg = require("./config");
var ExtractJwt = passportJWT.ExtractJwt;
var Strategy = passportJWT.Strategy;
var params = {
    secretOrKey: cfg.jwtSecret,
    jwtFromRequest: ExtractJwt.fromHeader('jwt')
};

module.exports = function() {
    var strategy = new Strategy(params, function(payload, done) {
        Employe.findOne({ username: payload.username }, function(err, emp) {
            if (emp) {
                return done(null, {
                    username: emp.username
                });
            } else {
                return done(new Error("emp not found"), null);
            }
        });

    });
    passport.use(strategy);
    return {
        initialize: function() {
            return passport.initialize();
        },
        authenticate: function() {
            return passport.authenticate("jwt", cfg.jwtSession);
        }
    };
};
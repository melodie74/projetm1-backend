var express = require('express');
var app = express();
var config = require('./config');
var nodemailer = require('nodemailer');

var hobbiesRoutes = require('./routes/hobbies');
var clientsRoutes = require('./routes/clients');
var employesRoutes = require('./routes/employes');
var eventsRoutes = require('./routes/events');
var tokenRoutes = require('./routes/token');
var isAuthRoutes = require('./routes/isAuth');
var auth = require("./auth.js")();


var mongoose = require('mongoose');
var bodyParser = require('body-parser');

mongoose.connect('mongodb://127.0.0.1/' + config.DATABASE_NAME);

/** MIDDLEWARE **/

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
app.use(function (req, res, next) {
        res.type('application/json');
        res.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, jwt, JWT");
    next();
});
app.use(auth.initialize());


/** ROUTES DEFINITIONS **/
hobbiesRoutes.setup(app);
clientsRoutes.setup(app);
employesRoutes.setup(app);
eventsRoutes.setup(app);
tokenRoutes.setup(app,auth);
isAuthRoutes.setup(app);


/** SERVER START **/
app.listen(3000, function () {
    console.log('API started on port',config.SERVER_PORT);
    var db = mongoose.connection;
    db.on('error', function(err) {
        console.log('Mongodb error' , err);
    });
    db.once('open', function() {
        console.log('Mongodb connected');
    });
});
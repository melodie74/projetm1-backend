var mongoose = require('mongoose');
var config = require('./config');

var Hobby = require('./models/hobby');
var Client = require('./models/client');
var Employe = require('./models/employe');
var Event = require('./models/event');


mongoose.connect('mongodb://127.0.0.1/' + config.DATABASE_NAME);

var db = mongoose.connection;


db.once('open', function() {
    console.log('Mongodb connected');

    console.log('Seeding...');

    Hobby.remove({},function (e,a) {
    });
    Hobby.collection.insert([
        {"name" : "Réalitée augmentée"},
        {"name" : "Internet des objets"},
        {"name" : "Réalité virtuelle"},
        {"name" : "Mobilité"}
    ]);

    Client.remove({},function (e,a) {
        });

    Employe.remove({},function (e,a){
    });
    Employe.collection.insert([
            {"username": "sdf" , "password" : "sdfsdf" , "isAdmin" : true},
        {"username": "abc" , "password" : "abcabc" , "isAdmin" : false}
    ]);


    mongoose.connection.close();
});
var mongoose = require('mongoose');


var eventSchema = mongoose.Schema({
        title: String,
        start: String,
        end: String,
        allDay: Boolean,
        email: String
});


module.exports = mongoose.model('events', eventSchema);


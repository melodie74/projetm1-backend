var mongoose = require('mongoose');


var hobbySchema = mongoose.Schema({
    lastname: String,
    firstname: String,
    email: String,
    hobby: String
});


module.exports = mongoose.model('clients', hobbySchema);

var mongoose = require('mongoose');


var employeSchema = mongoose.Schema({
    username: String,
    password: { type: String, select: false },
    isAdmin : Boolean,
    expiration_time : Date
});


module.exports = mongoose.model('employes', employeSchema);


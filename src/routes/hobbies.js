var Hobby = require('../models/hobby');

exports.setup = function (app) {

    app.get('/hobbies', function (req, res) {
        Hobby.find({}, function (err,hobbies) {
            res.json(hobbies);
        });
    });

    app.get('/hobbies/:id', function (req, res) {
        Hobby.findById(req.params.id, function (err,hobby) {
            if (hobby) {
                res.json(hobby);
            } else {
                res.sendStatus(404);
            }
        });
    });




};
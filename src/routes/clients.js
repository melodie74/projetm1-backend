var Client = require('../models/client');

exports.setup = function (app) {

    app.get('/clients', function (req, res) {
        Client.find({}, function (err,clients) {
            res.json(clients);
        });
    });

    app.get('/clients/:id', function (req, res) {
        Client.findById(req.params.id, function (err,client) {
            if (client) {
                res.json(client);
            } else {
                res.sendStatus(404);
            }
        });
    });

    app.post('/clients', function (req, res){
        var newClient = Client(req.body);
        newClient.save(function(err) {
          if (err) throw err;

          console.log('User created!');
          res.json(newClient);
        });
    });
};
var Employe = require('../models/employe.js');
var cfg = require("../config.js");
var jwt = require("jwt-simple");

exports.setup = function (app,auth) {


    app.post("/token", function (req, res) {

        if (req.body.username && req.body.password) {

            var username = req.body.username;
            var password = req.body.password;

            Employe.findOne({ username: username, password : password }, function(err, emp) {

                if(emp){
                    var payload = {
                        username: emp.username
                    };
                    var token = jwt.encode(payload, cfg.jwtSecret);

                    // Set expiration time of the token
                    var dateTemp1  = new Date();
                    emp.expiration_time = new Date();
                    var dateHours = dateTemp1.getHours();
                    var dateTemp3 = dateHours.toString();
                    var hours = Number(dateTemp3);
                    if(hours + 1 == 24){
                        hours = 0;
                    }else{
                        hours += 1;
                    }
                    emp.expiration_time.setHours(hours);

                    emp.save(function (err) {
                        if(err) {
                            console.error('ERROR!');
                        }
                    });

                    res.json({
                        token: token,
                        employe : emp
                    });
                }else {
                    res.sendStatus(401);
                }

            });

        } else {
            res.sendStatus(401);
        }
    });

}
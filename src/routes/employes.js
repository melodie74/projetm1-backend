var Employe = require('../models/employe');

exports.setup = function (app) {

    app.get('/employes', function (req, res) {
        Employe.find({}, function (err,employes) {
            res.json(employes);
        });
    });

    app.get('/employes/:id', function (req, res) {
        Employe.findById(req.params.id, function (err,employe) {
            if (employe) {
                res.json(employe);
            } else {
                res.sendStatus(404);
            }
        });
    });
};
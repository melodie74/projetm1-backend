/**
 * Created by melodiepellet on 26.01.17.
 */
var Event = require('../models/event');
var nodemailer = require('nodemailer');

var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'chivot.alex@gmail.com',
        pass: 'alex150993'
    }
});


exports.setup = function (app) {

    app.get('/events', function (req, res) {
        Event.find({}, function (err, events) {
            res.json(events);
        });
    });

    app.get('/events/:id', function (req, res) {
        Event.findById(req.params.id, function (err,event) {
            if (event) {
                res.json(event);
            } else {
                res.sendStatus(404);
            }
        });
    });


    app.post('/events', function (req, res){
        var newEvent = Event(req.body);

        // Envoie des mails aux invités
        var mails = newEvent.email;
        mails = mails.replace(/\s/g, '');
        var listMails = mails.split(';');
        var mailOptions;
        // Traitement de la date et l'heure pour avoir un visuel correct
        // Format de base AAAA-MM-DDTHH:MM:SS.MSZ
        var dateHeure = newEvent.start.split('T');
        // On récupère juste l'heure pour pouvoir afficher uniquement l'heure
        var heure = dateHeure[1].split(':');
        if(heure[0] != 10){
            heure[0] = heure[0].replace("0","");
        }

        for(i = 0; i < listMails.length; i++){
            mailOptions = {
                from: '"Alex Chivot" <chivot.alex@gmail.com>', // sender address
                to: listMails[i], // list of receivers
                subject: 'Test', // Subject line
                html: '<p>Bonjour, <br><br> Nous vous invitons à participer à une visite de notre lab le ' + dateHeure[0] + ' à ' + heure[0] + ' h <br><br><br> L\'équipe de capgemini </p>' // html body
            };
            transporter.sendMail(mailOptions,  function (error, info){
                if (error) {
                    return console.log(error);
                }
                console.log('Message %s sent: %s', info.messageId, info.response);
            });
        }
        newEvent.save(function(err) {
            if (err) throw err;
            console.log('event create!');
            res.json(newEvent);
        });
    });

    app.put('/events/:id', function (req, res) {
        Event.findById(req.params.id, function (err,event) {
            if ( event ) {
                event.title = req.body.title;
                event.save(function (err) {
                    if (err) throw err;
                    res.json({message:'Event updated!'});
                 })
            }
            else res.sendStatus(404);
        })
     });





    app.delete('/events/:id', function (req, res) {
        Event.remove({_id : req.params.id}, function (err,event) {
            if (err){
                res.sendStatus(404);
            }
        });
    })

};
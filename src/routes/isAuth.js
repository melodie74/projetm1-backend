var cfg = require("../config.js");
var jwt = require("jwt-simple");
var Employe = require('../models/employe');

exports.setup = function (app) {


    app.post("/protected" ,function(req, res) {

        var tokenjwt = req.body.jwt;
        var decoded = jwt.decode(tokenjwt, cfg.jwtSecret);
        var date = new Date();
        // Trying to look for the decoded username from the token in the DB
        Employe.findOne({ username: decoded.username}, function(err, emp) {
            if(emp){
                if(emp.expiration_time >= date) {
                    res.json(emp);
                }
                else {
                    res.sendStatus(402);
                }
            }
        });
    });
}
# My project's README

server web : expressJS 
connecteur mongo : mongoose

prérequis: serveur mongoDB lancé avec base de donnée "cap" 
Vérifié dans etc/host si localhost est bien sur 
installation : npm install 
seed: node src/seed.js 
demarrer serveur: node src/app.js

port par defaut : 3000 dans config.js